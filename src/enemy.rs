use macroquad::prelude::*;
use std::slice::IterMut;

use crate::game::GameLevel;

pub struct Enemy {
    pub position: Vec2,
    active: bool,
    left_to_right: bool,
}

impl Enemy {
    pub fn new() -> Enemy {
        let height = rand::gen_range(100., 200.);
        let left_to_right = height < 150.;

        let x_pos = if left_to_right {
            0.
        } else {
            screen_width() - Enemies::ENEMY_WIDTH
        };

        return Enemy {
            position: Vec2::new(x_pos, height),
            active: true,
            left_to_right,
        };
    }

    pub fn move_en(&mut self, speed: f32) {
        if self.left_to_right {
            self.position.x += speed;
            return;
        }

        self.position.x -= speed;
    }

    pub fn kill(&mut self) {
        self.active = false
    }
}

pub struct Enemies {
    current: Vec<Enemy>,
    texture: Texture2D,
}

impl Enemies {
    pub const ENEMY_WIDTH: f32 = 45.;
    pub const ENEMY_HEIGHT: f32 = 61.;

    pub async fn new() -> Enemies {
        #[cfg(not(target_family = "wasm"))]
        let texture = load_texture("assets/pizzaslice.png").await.unwrap();

        #[cfg(target_family = "wasm")]
        let texture = load_texture("/assets/pizzaslice.png").await.unwrap();

        return Enemies {
            current: Vec::new(),
            texture,
        };
    }

    pub fn add(&mut self) {
        self.current.push(Enemy::new())
    }

    pub fn update(&mut self, game_level: &GameLevel) {
        let speed = match game_level {
            GameLevel::Lvl1 => 3.,
            GameLevel::Lvl2 => 5.,
            GameLevel::Lvl3 => 7.,
            GameLevel::Lvl4 => 9.,
        };

        if self.current.len() > 0 {
            for enemy in self.current.iter_mut() {
                if enemy.active {
                    // Draw the hitbox
                    /* draw_rectangle(
                        enemy.position.x,
                        enemy.position.y,
                        Enemies::ENEMY_WIDTH,
                        Enemies::ENEMY_HEIGHT,
                        MAGENTA,
                    ); */

                    draw_texture_ex(
                        self.texture,
                        enemy.position.x,
                        enemy.position.y,
                        WHITE,
                        DrawTextureParams {
                            dest_size: Some(Vec2::new(Enemies::ENEMY_WIDTH, Enemies::ENEMY_HEIGHT)),
                            ..DrawTextureParams::default()
                        },
                    );

                    enemy.move_en(speed);
                }
            }

            // Only keep still active enemies that are still in the viewport.
            self.current.retain(|x| {
                x.active
                    && ((x.left_to_right && x.position.x < screen_width() - Enemies::ENEMY_WIDTH)
                        || (!x.left_to_right && x.position.x >= 0.))
            });
        }
    }

    pub fn get_mut(&mut self) -> IterMut<'_, Enemy> {
        self.current.iter_mut()
    }

    pub fn amount(&self) -> usize {
        self.current.len()
    }
}
