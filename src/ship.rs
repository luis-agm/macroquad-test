use macroquad::math::Vec2;
use macroquad::prelude::*;

use crate::shots::Shots;

static STEP: f32 = 4.;

pub struct Ship {
    position: Vec2,
    texture: Texture2D,
    size: Vec2,
}

impl Ship {
    pub async fn new(init_position: Vec2) -> Ship {
        // This makes sense since there's only one ship. Should not reimport textures for similar entities.
        #[cfg(target_family = "wasm")]
        let texture = load_texture("/assets/spaceship.png").await.unwrap();
        #[cfg(not(target_family = "wasm"))]
        let texture = load_texture("assets/spaceship.png").await.unwrap();
        let size = Vec2::new(88., 48.);
        let position = Vec2::new(init_position.x - size.x / 2., init_position.y - size.y / 2.);

        return Ship {
            position,
            texture,
            size,
        };
    }

    pub fn update(&self) {
        draw_texture_ex(
            self.texture,
            self.position.x,
            self.position.y,
            WHITE,
            DrawTextureParams {
                dest_size: Some(Vec2::new(self.size.x, self.size.y)),
                ..Default::default()
            },
        );
    }

    pub fn tip_position(&self) -> Vec2 {
        return Vec2::new(
            self.position.x - Shots::SHOT_SIZE.0 + self.size.x / 2.,
            self.position.y - Shots::SHOT_SIZE.1,
        );
    }

    pub fn move_up(&mut self) {
        if self.position.y > 350. {
            self.position.y -= STEP;
        }
    }

    pub fn move_down(&mut self) {
        if self.position.y < screen_height() - self.size.y {
            self.position.y += STEP;
        }
    }

    pub fn move_right(&mut self) {
        if self.position.x < screen_width() - self.size.x {
            self.position.x += STEP;
        }
    }

    pub fn move_left(&mut self) {
        if self.position.x > 0. {
            self.position.x -= STEP;
        }
    }
}
