use macroquad::prelude::*;
use macroquad::ui::{root_ui, Skin};

/*
    Examples: https://macroquad.rs/examples
    Code for the UI example: https://github.com/not-fl3/macroquad/blob/master/examples/ui_skins.rs
*/

pub fn setup_ui(font: &[u8]) {
    let skin = create_skin(font);
    root_ui().push_skin(&skin);
}

fn create_skin(font: &[u8]) -> Skin {
    let label_style = root_ui()
        .style_builder()
        .font(font)
        .unwrap()
        .text_color(WHITE)
        .font_size(20)
        .build();

    /* let window_style = root_ui()
    .style_builder()
    .color(Color::from_rgba(0, 0, 0, 125))
    .background_margin(RectOffset::new(20.0, 20.0, 10.0, 10.0))
    .margin(RectOffset::new(-20.0, -30.0, 0.0, 0.0))
    .build(); */

    let button_style = root_ui()
        .style_builder()
        .background_margin(RectOffset::new(37.0, 37.0, 5.0, 5.0))
        .margin(RectOffset::new(0.0, 0.0, 0.0, 0.0))
        .font(font)
        .unwrap()
        .text_color(DARKGRAY)
        .font_size(24)
        .build();

    /* let editbox_style = root_ui()
    .style_builder()
    .background_margin(RectOffset::new(0., 0., 0., 0.))
    .font(include_bytes!("../assets/fonts/pressStart.ttf"))
    .unwrap()
    .text_color(Color::from_rgba(120, 120, 120, 255))
    .color_selected(Color::from_rgba(190, 190, 190, 255))
    .font_size(50)
    .build(); */

    Skin {
        //editbox_style,
        //window_style,
        label_style,
        button_style,
        ..root_ui().default_skin()
    }
}
