use macroquad::prelude::*;

pub struct Background {
    texture: Texture2D,
    framesize: Vec2,
    frames: usize,
    current_frame: usize,
    last_update: f64,
}

impl Background {
    pub fn new(texture: Texture2D, framesize: Vec2, frames: usize) -> Background {
        Background {
            texture,
            framesize,
            frames,
            current_frame: 0,
            last_update: 0.,
        }
    }

    pub fn update(&mut self) {
        if get_time() - self.last_update > 0.5 {
            if self.current_frame < self.frames {
                self.current_frame += 1
            } else {
                self.current_frame = 0
            }

            self.last_update = get_time();
        }
        self.draw();
    }

    pub fn draw(&self) {
        let frame_start = Vec2::new(self.framesize.x * self.current_frame as f32, 0.);
        draw_texture_ex(
            self.texture,
            0.,
            0.,
            WHITE,
            DrawTextureParams {
                source: Some(Rect::new(
                    frame_start.x,
                    frame_start.y,
                    self.framesize.x,
                    self.framesize.y,
                )),
                dest_size: Some(Vec2::new(screen_width(), screen_height())),
                ..DrawTextureParams::default()
            },
        );
    }
}
