mod background;
mod enemy;
mod game;
mod menu;
mod ship;
mod shots;
mod ui;

use background::Background;
use game::{Game, GameState};
use macroquad::prelude::*;
use menu::Menu;
//use std::{thread, time::Duration};

fn window_conf() -> Conf {
    Conf {
        window_title: "Space Pizza Attack".to_owned(),
        window_height: 720,
        window_width: 1280,
        ..Default::default()
    }
}

#[macroquad::main(window_conf)]
async fn main() {
    //load Splash Screen
    #[cfg(not(target_family = "wasm"))]
    let splash_img = load_texture("assets/splash-screen-opt.png").await.unwrap();
    #[cfg(target_family = "wasm")]
    let splash_img = load_texture("/images/splash-screen-opt.png").await.unwrap();

    //Load font:
    let font = include_bytes!("../assets/fonts/pressStart.ttf");
    let mquad_font = load_ttf_font_from_bytes(font).unwrap();

    //Game Instance
    let mut main_game = Game::new(Some(GameState::StartMenu), Some(mquad_font)).await;

    //UI
    ui::setup_ui(font);

    //Menu
    let menu = Menu::new(mquad_font);

    //Background
    //Animated:
    // let bg_texture = load_texture("assets/sprites/space_bg2.png").await.unwrap();
    // let mut background = Background::new(bg_texture, Vec2::new(64., 64.), 3);
    //Regular HD:
    #[cfg(target_family = "wasm")]
    let bg_texture = load_texture("/assets/bg-v3-opt.png").await.unwrap();
    #[cfg(not(target_family = "wasm"))]
    let bg_texture = load_texture("assets/bg-v3-opt.png").await.unwrap();

    let mut background = Background::new(bg_texture, Vec2::new(1280., 720.), 1);

    loop {
        match main_game.state {
            GameState::StartMenu => {
                clear_background(DARKGRAY);
                draw_texture(splash_img, 0., 0., WHITE);
                if menu.draw_start() {
                    main_game.play().await;
                }
            }
            GameState::Paused => {
                clear_background(DARKGRAY);
                draw_texture(splash_img, 0., 0., WHITE);
                if menu.draw_pause() {
                    main_game.unpause();
                };
            }
            GameState::GameOver => {
                clear_background(DARKGRAY);
                draw_texture(splash_img, 0., 0., WHITE);
                if menu.draw_game_over(main_game.score) {
                    main_game.play().await;
                };
            }
            GameState::Playing => {
                background.update();

                //Game controls
                if is_key_released(KeyCode::Escape) || is_key_released(KeyCode::P) {
                    main_game.pause();
                }

                // Ship controls
                if is_key_down(KeyCode::Right) {
                    main_game.ship.move_right();
                }
                if is_key_down(KeyCode::Left) {
                    main_game.ship.move_left();
                }
                if is_key_down(KeyCode::Down) {
                    main_game.ship.move_down();
                }
                if is_key_down(KeyCode::Up) {
                    main_game.ship.move_up();
                }

                if is_key_released(KeyCode::Space) {
                    main_game.shoot();
                }

                // Trigger main game update
                main_game.update();
            }
        }

        //Sleep not supported by the wasm thingy
        //thread::sleep(Duration::from_millis(10));
        next_frame().await;
    }
}
