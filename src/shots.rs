use std::slice::Iter;

use macroquad::prelude::*;

pub struct Shot {
    // Should refactor this to be a Vec2
    pub position: (f32, f32),
}

impl Shot {
    pub fn new(origin: (f32, f32)) -> Shot {
        return Shot { position: origin };
    }
    pub fn move_shot(&mut self) {
        self.position.1 -= 6.;
    }
}

pub struct Shots {
    current: Vec<Shot>,
    texture: Texture2D,
}

impl Shots {
    pub const SHOT_SIZE: (f32, f32) = (12., 24.);

    pub async fn new() -> Shots {
        #[cfg(target_family = "wasm")]
        let texture = load_texture("/assets/sprites/projectiles.png")
            .await
            .unwrap();

        #[cfg(not(target_family = "wasm"))]
        let texture = load_texture("assets/sprites/projectiles.png")
            .await
            .unwrap();

        return Shots {
            current: Vec::new(),
            texture,
        };
    }

    pub fn update(&mut self) {
        if self.current.len() > 0 {
            for shot in self.current.iter_mut() {
                //Draw hitbox
                /* draw_rectangle(
                    shot.position.0,
                    shot.position.1,
                    Shots::SHOT_SIZE.0,
                    Shots::SHOT_SIZE.1,
                    GREEN,
                ); */

                draw_texture_ex(
                    self.texture,
                    shot.position.0,
                    shot.position.1,
                    WHITE,
                    DrawTextureParams {
                        source: Some(Rect::new(2., 9., 3., 6.)),
                        dest_size: Some(Vec2::new(Shots::SHOT_SIZE.0, Shots::SHOT_SIZE.1)),
                        ..DrawTextureParams::default()
                    },
                );

                shot.move_shot();
            }

            self.current.retain(|x| x.position.1 > -10.);
        }
    }

    pub fn add_shot(&mut self, origin: (f32, f32)) {
        let new_shot: Shot = Shot::new(origin);
        self.current.push(new_shot)
    }

    pub fn get(&self) -> Iter<'_, Shot> {
        self.current.iter()
    }
}
