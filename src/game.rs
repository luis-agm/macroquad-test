use macroquad::prelude::*;

use crate::enemy::Enemies;
use crate::ship::Ship;
use crate::shots::Shots;

pub enum GameState {
    Playing,
    Paused,
    StartMenu,
    GameOver,
}

pub enum GameLevel {
    Lvl1,
    Lvl2,
    Lvl3,
    Lvl4,
}

impl GameLevel {
    fn get_lvl_num(&self) -> u8 {
        return match self {
            GameLevel::Lvl1 => 1,
            GameLevel::Lvl2 => 2,
            GameLevel::Lvl3 => 3,
            GameLevel::Lvl4 => 4,
        };
    }
}

pub struct Game {
    pub state: GameState,
    pub ship: Ship,
    shots: Shots,
    shots_left: usize,
    enemies: Enemies,
    lastenemy: f64,
    pub score: usize,
    level: GameLevel,
    font: Font,
}

impl Game {
    const ENEMY_GAP: f64 = 2.;
    const SHOT_LIMIT: usize = 10;

    pub async fn new(init_state: Option<GameState>, font: Option<Font>) -> Game {
        let initial_ship_position = Vec2::new(screen_width() / 2. - 13., screen_height() - 100.);

        //UI stuff
        let font = font.unwrap();

        //Entities
        // Better way but needs nightly
        /*
        let enemies_load = Enemies::new();
        let shots_load = Shots::new();

        let (enemies, shots) = join!(enemies_load, shots_load).await;
        */

        // Slower way
        let enemies = Enemies::new().await;
        let shots = Shots::new().await;

        match init_state {
            Some(state) => {
                return Game {
                    state,
                    ship: Ship::new(initial_ship_position).await,
                    shots,
                    shots_left: Game::SHOT_LIMIT,
                    enemies,
                    lastenemy: get_time(),
                    score: 0usize,
                    level: GameLevel::Lvl1,
                    font,
                }
            }
            None => {
                return Game {
                    state: GameState::StartMenu,
                    ship: Ship::new(initial_ship_position).await,
                    shots,
                    shots_left: Game::SHOT_LIMIT,
                    enemies,
                    lastenemy: get_time(),
                    score: 0usize,
                    level: GameLevel::Lvl1,
                    font,
                }
            }
        }
    }

    pub fn update(&mut self) {
        self.enemies.update(&self.level);
        self.ship.update();
        self.shots.update();

        //Check if there's a hit and if level shoulb be bumped.
        self.collition_check();
        self.lvl_up_check();

        //Spawn enemies every ENEMY_GAP seconds.
        if get_time() - self.lastenemy > Game::ENEMY_GAP && self.enemies.amount() < 5 {
            self.enemies.add();
            self.lastenemy = get_time();
        }

        //GameOver for lack of shots left.
        if self.shots_left == 0usize && self.shots.get().len() == 0usize {
            self.state = GameState::GameOver;
            return ();
        }

        self.draw_ui();
    }

    fn draw_ui(&self) {
        //HUD Container
        let container_color = Color::new(0.00, 0.00, 0.00, 0.40);
        draw_rectangle(0., 0., screen_width(), 28., container_color);

        //HUD
        let score_info = format!("Score: {}", self.score);
        let level_info = format!("Level: {}", self.level.get_lvl_num());
        let shots_left_info = format!("Shots left: {}", self.shots_left);

        //Font config
        let params = TextParams {
            font: self.font,
            color: WHITE,
            font_size: 16,
            ..TextParams::default()
        };

        draw_text_ex(&score_info, screen_width() - 150., 24., params);

        draw_text_ex(&level_info, screen_width() / 2., 24., params);

        draw_text_ex(&shots_left_info, 20., 24., params);
    }

    fn collition_check(&mut self) {
        let mut acc_score = 0usize;

        for sh in self.shots.get() {
            // 200 is the max height where enemies are rendered.
            // No need to iterate if the shot is bellow this height.
            if sh.position.1 < (200. + Enemies::ENEMY_HEIGHT) {
                for en in self.enemies.get_mut() {
                    {
                        if sh.position.0 < en.position.x + Enemies::ENEMY_WIDTH
                            && sh.position.0 + Shots::SHOT_SIZE.0 > en.position.x
                            && sh.position.1 < en.position.y + Enemies::ENEMY_HEIGHT
                            && Shots::SHOT_SIZE.1 + sh.position.1 > en.position.y
                        {
                            en.kill();
                            acc_score += 5usize;
                            self.shots_left += 2;
                        }
                    }
                }
            }
        }

        self.up_score(acc_score);
    }

    fn lvl_up_check(&mut self) {
        match self.score {
            25 => self.level = GameLevel::Lvl2,
            50 => self.level = GameLevel::Lvl3,
            75 => self.level = GameLevel::Lvl4,
            _ => (),
        }
    }

    pub fn shoot(&mut self) {
        if self.shots_left > 0 {
            let origin = (self.ship.tip_position().x, self.ship.tip_position().y);
            self.shots.add_shot(origin);
            self.shots_left -= 1;
        }
    }

    fn up_score(&mut self, score: usize) {
        self.score += score
    }

    pub fn pause(&mut self) {
        self.state = GameState::Paused
    }

    pub fn unpause(&mut self) {
        self.state = GameState::Playing
    }

    pub async fn play(&mut self) {
        self.reset_game().await;
        self.state = GameState::Playing
    }

    async fn reset_game(&mut self) {
        let initial_ship_position = Vec2::new(screen_width() / 2. - 13., screen_height() - 100.);

        self.ship = Ship::new(initial_ship_position).await;
        self.enemies = Enemies::new().await;
        self.score = 0usize;
        self.shots = Shots::new().await;
        self.shots_left = Game::SHOT_LIMIT;
        self.lastenemy = get_time();
    }
}
