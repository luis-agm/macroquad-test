use macroquad::{prelude::*, ui::root_ui};

pub struct Menu {
    font: Font,
}

// Boolean should be enough for now. With more options an enum will be necessary... maybe.

impl Menu {
    pub fn new(font: Font) -> Menu {
        Menu { font }
    }

    pub fn draw_start(&self) -> bool {
        let label = "Click start or press the Space Bar";
        let controls_label = "Move: ←↑↓→ |  Shoot: <Space> | Pause: <P>, <Esc>";
        let button_label = "START";

        // Font size and button margin set at Skin level at src/ui.rs, hardcoded here because I'm lazy.
        let label_position = self.center_text(label, -1.);
        let controls_label_position = self.center_text(controls_label, 0.5);
        let button_position = self.center_button(button_label, 2.);

        let mut root_ui = root_ui();

        root_ui.label(label_position, label);
        root_ui.label(controls_label_position, controls_label);

        return root_ui.button(button_position, button_label) || is_key_released(KeyCode::Space);
    }

    pub fn draw_pause(&self) -> bool {
        let label = "Game Paused";
        let button_label = "CONTINUE";

        // Font size and button margin set at Skin level at src/ui.rs, hardcoded here because I'm lazy.
        let label_position = self.center_text(label, 0.);
        let button_position = self.center_button(button_label, 2.);

        let mut root_ui = root_ui();
        let mut btn_clicked = false;

        root_ui.label(label_position, label);

        if root_ui.button(button_position, button_label) {
            btn_clicked = true
        };

        return btn_clicked || is_key_released(KeyCode::Escape) || is_key_released(KeyCode::P);
    }

    pub fn draw_game_over(&self, final_score: usize) -> bool {
        let you_lose_label = format!("Game Over! Final score: {}", final_score);
        let restart_label = "Want to play again?";
        let button_label = "START";

        let you_lose_position = self.center_text(&you_lose_label, -2.);
        let label_position = self.center_text(&restart_label, 0.);
        let button_position = self.center_button(button_label, 2.);

        let mut root_ui = root_ui();

        root_ui.label(you_lose_position, &you_lose_label);
        root_ui.label(label_position, restart_label);

        return root_ui.button(button_position, button_label) || is_key_released(KeyCode::Space);
    }

    // Don't like to live here but it needs the font and I don't want to reimport it every time.
    fn center_text(&self, label: &str, offset: f32) -> Vec2 {
        let text_size = measure_text(label, Some(self.font), 20, 1.);

        Vec2::new(
            screen_width() / 2. - (text_size.width / 2.),
            (screen_height() / 2.) + (text_size.height * offset),
        )
    }

    fn center_button(&self, label: &str, offset: f32) -> Vec2 {
        let text_size = measure_text(label, Some(self.font), 20, 1.);

        // 37. is the background margin on each side (L R) of the button set in the ui module.
        Vec2::new(
            screen_width() / 2. - ((text_size.width + 74.) / 2.),
            (screen_height() / 2.) + (text_size.height * offset),
        )
    }
}
